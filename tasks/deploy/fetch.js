var utils = require('shipit-utils');
var chalk = require('chalk');
var mkdirp = require('mkdirp');
var Promise = require('bluebird');

/**
 * Fetch task.
 * - Create workspace.
 * - Fetch repository.
 * - Checkout commit-ish.
 */

module.exports = function (gruntOrShipit) {
  utils.registerTask(gruntOrShipit, 'deploy:fetch', task);

  function task() {
    var shipit = utils.getShipit(gruntOrShipit);

    return createWorkspace()
    .then(unarchiveArtifact)
    .then(function () {
      shipit.emit('fetched');
    });

    /**
     * Create workspace.
     */

    function createWorkspace() {
      function create() {
        shipit.log('Create workspace "%s"', shipit.config.workspace);
        return Promise.promisify(mkdirp)(shipit.config.workspace)
        .then(function () {
          shipit.log(chalk.green('Workspace created.'));
        });
      }

      if (shipit.config.shallowClone) {
        shipit.log('Deleting existing workspace "%s"', shipit.config.workspace);
        return shipit.local('rm -rf ' + shipit.config.workspace)
        .then(create);
      }

      return create();
    }

    /**
     * Unarchive
     */
    function unarchiveArtifact() {
        shipit.log('Unarchiving artifact "%s" => "%s"',
            shipit.config.artifact, shipit.config.workspace);

        var cmd = 'tar -C ' + shipit.config.workspace + ' -xf ' + shipit.config.artifact;
        return shipit.local(cmd)
        .then(function () {
            shipit.log(chalk.green('Unarchived.'));
        });
    }
  }
};
